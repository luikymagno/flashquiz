package com.example.flashquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thoughtworks.xstream.XStream;

import java.util.Iterator;
import java.util.Vector;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private Vector<Quiz> quizzes;
    private int quizIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Intent info = getIntent();
        quizIndex = info.getIntExtra("quiz_index", 0);
        quizzes = recoveryQuizzes();
        TextView title = findViewById(R.id.textView);
        title.setText(quizzes.get(quizIndex).getTitle());
    }

    @Override
    protected void onStart() {
        super.onStart();
        quizzes = recoveryQuizzes();
        this.showCards();
    }

    public void createCard(View view) {
        Intent changeContext = new Intent();
        changeContext.setClass(this, CreateCard.class);
        changeContext.putExtra("quiz_index", quizIndex);
        startActivity(changeContext);
    }

    public void playQuiz(View view) {
    }

    public void showCards() {
        Iterator it = quizzes.get(quizIndex).getCartesIterator();
        if(it.hasNext()) {
            LinearLayout layout = findViewById(R.id.activity_my_quizzes);
            layout.removeViews(0, layout.getChildCount());
            Button b;
            while(it.hasNext()) {
                b = new Button(this);
                Carte card = (Carte) it.next();
                b.setText(card.getQuestion());
                b.setOnClickListener(this);
                layout.addView(b);
            }
        }
        else {
            TextView textView = findViewById(R.id.info);
            textView.setText("You don't have cards yet");
        }
    }

    @Override
    public void onClick(View v) {
    }

    public Vector<Quiz> recoveryQuizzes() {
        Vector<Quiz> quizzes;
        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        if(sp.contains("my_quizzes")) {
            XStream xStream = new XStream();
            String xml = sp.getString("my_quizzes", "");
            quizzes = (Vector<Quiz>) xStream.fromXML(xml);
        }
        else {
            quizzes = new Vector<Quiz>();
        }
        return quizzes;
    }
}
