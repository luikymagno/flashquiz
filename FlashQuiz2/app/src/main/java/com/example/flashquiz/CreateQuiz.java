package com.example.flashquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thoughtworks.xstream.XStream;

import java.util.Iterator;
import java.util.Vector;

public class CreateQuiz extends AppCompatActivity {

    private Vector<Quiz> quizzes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);
        quizzes = recoveryQuizzes();
        quizzes.add(new Quiz(""));
        this.saveQuizzes(quizzes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        quizzes = this.recoveryQuizzes();
        this.showCards();
    }

    public void createCard(View view) {
        Intent changeContext = new Intent();
        changeContext.setClass(this, CreateCard.class);
        changeContext.putExtra("quiz_index", quizzes.size()-1);
        startActivity(changeContext);
    }

    public void finishCreation(View view) {
        EditText et = findViewById(R.id.title);
        quizzes.get(quizzes.size()-1).changeTitle(et.getText().toString());
        saveQuizzes(quizzes);
        finish();
    }

    public Vector<Quiz> recoveryQuizzes() {
        Vector<Quiz> quizzes;
        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        if(sp.contains("my_quizzes")) {
            XStream xStream = new XStream();
            String xml = sp.getString("my_quizzes", "");
            quizzes = (Vector<Quiz>) xStream.fromXML(xml);
        }
        else {
            quizzes = new Vector<Quiz>();
        }
        return quizzes;
    }

    public void saveQuizzes(Vector<Quiz> quizzes) {
        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        XStream xstream = new XStream();
        String xml = xstream.toXML(quizzes);
        editor.putString("my_quizzes", xml);
        editor.apply();
    }

    public void showCards() {
        Iterator it = quizzes.get(quizzes.size()-1).getCartesIterator();
        Log.i("teste", "ola1");
        if(it.hasNext()) {
            Log.i("teste", "ola");
            LinearLayout layout = findViewById(R.id.activity_my_quizzes);
            layout.removeViews(0, layout.getChildCount());
            Button b;
            while(it.hasNext()) {
                b = new Button(this);
                Carte card = (Carte) it.next();
                b.setText(card.getQuestion());
                layout.addView(b);
            }
        }
        else {
            Log.i("teste", "rola");
            TextView textView = findViewById(R.id.info);
            textView.setText("This Quiz doesn't have cards yet");
        }
    }
}
