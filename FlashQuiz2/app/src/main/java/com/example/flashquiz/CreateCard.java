package com.example.flashquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.thoughtworks.xstream.XStream;

import java.util.Vector;

public class CreateCard extends AppCompatActivity {

    private Carte card;
    private int quizIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_card);

        Intent info = getIntent();
        quizIndex = info.getIntExtra("quiz_index", -1);
    }

    public void newField(View view) {
        LinearLayout layout = findViewById(R.id.my_layout);
        EditText et = new EditText(this);
        et.setHint("Wrong Answer");
        layout.addView(et);
    }

    public void createCard(View view) {
        LinearLayout layout = findViewById(R.id.my_layout);
        Vector<String> wrongAnswers = new Vector<String>();
        for(int i = 0; i < layout.getChildCount(); i++) {
            EditText et = (EditText) layout.getChildAt(i);
            wrongAnswers.add(et.getText().toString());
        }
        EditText et;
        et = findViewById(R.id.question);
        String question = et.getText().toString();
        et = findViewById(R.id.right_answer);
        String rightAnswer = et.getText().toString();

        card = new Carte(question, rightAnswer, wrongAnswers);
        Vector<Quiz> quizzes = this.recoveryQuizzes();
        quizzes.get(quizIndex).addCard(card);
        XStream xstream = new XStream();
        this.saveQuizzes(quizzes);
        finish();
    }

    public Vector<Quiz> recoveryQuizzes() {
        Vector<Quiz> quizzes;
        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        if(sp.contains("my_quizzes")) {
            XStream xStream = new XStream();
            String xml = sp.getString("my_quizzes", "");
            quizzes = (Vector<Quiz>) xStream.fromXML(xml);
        }
        else {
            quizzes = new Vector<Quiz>();
        }
        return quizzes;
    }

    public void saveQuizzes(Vector<Quiz> quizzes) {
        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        XStream xstream = new XStream();
        String xml = xstream.toXML(quizzes);
        editor.putString("my_quizzes", xml);
        editor.apply();
    }
}
