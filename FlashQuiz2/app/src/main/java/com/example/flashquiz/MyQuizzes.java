package com.example.flashquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.thoughtworks.xstream.XStream;

import java.util.Vector;

public class MyQuizzes extends AppCompatActivity implements View.OnClickListener {

    private Vector<Quiz> quizzes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_quizzes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.recoveryQuizzes();
        this.showQuizzes();
    }

    public void onClick(View v) {
        Intent changeContext = new Intent();
        changeContext.setClass(this, QuizActivity.class);
        changeContext.putExtra("quiz_index", Integer.parseInt(v.getTag().toString()));
        startActivity(changeContext);
        Log.i("test", "here "+v.getTag().toString());
    }

    public void createQuiz(View view) {
        Intent changeContext = new Intent();
        changeContext.setClass(this, CreateQuiz.class);
        startActivity(changeContext);
    }

    public void recoveryQuizzes() {

        SharedPreferences sp = getSharedPreferences("quizzes", Context.MODE_PRIVATE);
        if(sp.contains("my_quizzes")) {
            XStream xStream = new XStream();
            String xml = sp.getString("my_quizzes", "");
            quizzes = (Vector<Quiz>) xStream.fromXML(xml);
        }
        else {
            quizzes = new Vector<Quiz>();
        }
    }

    public void showQuizzes() {
        if(quizzes.size() == 0) {
            TextView textView = findViewById(R.id.info);
            textView.setText("You don't have quizzes yet");
        }
        else {
            LinearLayout layout = findViewById(R.id.activity_my_quizzes);
            layout.removeViews(0, layout.getChildCount());
            Button b;
            for(int i = 0; i < quizzes.size(); i++) {
                b = new Button(this);
                b.setText(quizzes.get(i).getTitle());
                b.setTag(String.valueOf(i));
                b.setOnClickListener(this);
                layout.addView(b);
            }
        }
    }


}
