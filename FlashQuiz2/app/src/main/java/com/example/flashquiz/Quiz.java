package com.example.flashquiz;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

/**
 * Cette classe représente un jeu (un ensemble) de cartes de QCM.
 *
 * @author Virginie Galtier
 * @see Carte
 */
public class Quiz implements Serializable {

    private String title;
    // liste des "fiches - questions" composant le quiz
    private Vector<Carte> lesCartes;

    /**
     * Crée un nouveau quiz comportant 1 question
     */
    public Quiz(String title) {
        lesCartes = new Vector<Carte>();
        this.title = title;
    }

    public void addCard(Carte carte) {
        lesCartes.add(carte);
    }

    /**
     * Fournit un itérateur sur les questions du quiz
     *
     * @return itérateur sur les questions du quiz
     */
    public Iterator<Carte> getCartesIterator() {
        return lesCartes.iterator();
    }

    public String getTitle() {
        return title;
    }

    public void changeTitle(String title) {
        this.title = title;
    }
}
