package com.example.flashquiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void sharedQuizzes(View view) {
        //Intent changeContext = new Intent();
        //changeContext.setClass(this, Quizpartages.class);
        //startActivity(changeContext);
        //finish();
    }

    public void myQuizzes(View view) {
        Intent changeContext = new Intent();
        changeContext.setClass(this, MyQuizzes.class);
        startActivity(changeContext);
    }
}
